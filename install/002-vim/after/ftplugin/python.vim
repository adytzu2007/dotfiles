" Set tab and indent width
setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
" Expand tabs to spaces
setlocal expandtab

